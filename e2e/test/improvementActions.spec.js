import Issue from './pages/Issue'

describe ('Rekaizen', () => {
  it('creates an Improvement Action', () => {
    const description = "improvement action text"

    const issue = new Issue()
      .createIssue()
      .showImprovementActionForm()
      .fillImprovementAction("action")
      .cancelImprovementAction()
      .showImprovementActionForm()
      .fillImprovementAction(description)
      .addImprovementAction()
      .showImprovementActionForm()
      .fillImprovementAction("another action")
      .addImprovementAction()
      .deleteFirstImprovementAction()

    expect(issue.includes(description)).to.be.true
  })
})
