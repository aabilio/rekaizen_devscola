import Issue from './pages/Issue'

describe('Report',()=>{
    it('contains download summary button', () =>{
      const negative = 'a negative'
      const issue = new Issue()
      .createIssue()
      .openNegativeForm()
      .fillNegativeWith(negative)
      .confirmNegativeWithEnter()
      .cancelNegative()
      .clickCreateReport()

      expect(issue.containDownloadSummary()).to.be.true
    })
})
