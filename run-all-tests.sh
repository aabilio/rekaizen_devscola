#/bin/sh

docker-compose exec app npm run test-all
docker-compose exec app npm run build
docker-compose exec e2e npm run test-all
