import Bus from './infrastructure/bus'
import Containers from './containers'
import Actions from './api/actions'
import Services from './services'

const bus = new Bus()

Containers.improvementActions(bus).render()
Containers.lessonsLearned(bus).render()
Containers.goodPractices(bus).render()
Containers.positives(bus).render()
Containers.negatives(bus).render()
Containers.navbar(bus).render()
Containers.report(bus).render()
Containers.landing(bus).render()
Containers.issue(bus).render()

Services.issues(bus)
Services.improvementActions(bus)
Services.goodPractices(bus)
Services.positives(bus)
Services.reports(bus)

Actions.retrieveAnalyses(bus)
Actions.removeAnalysis(bus)
Actions.createNegative(bus)
Actions.createLessonLearned(bus)
Actions.retrieveLessonsLearned(bus)
Actions.retrieveTimeBoxes(bus)
Actions.translateAll(bus)
