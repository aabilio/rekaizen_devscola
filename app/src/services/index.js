import ImprovementActions from './ImprovementActions'
import GoodPractices from './goodPractices'
import Positives from './positives'
import Reports from './reports'
import Issues from './issues'

class Services {
  static improvementActions(bus) {
    return new ImprovementActions(bus)
  }

  static goodPractices(bus) {
    return new GoodPractices(bus)
  }

  static positives(bus) {
    return new Positives(bus)
  }

  static reports(bus) {
    return new Reports(bus)
  }

  static issues(bus) {
    return new Issues(bus)
  }
}

export default Services
