class Renderer {
  constructor(callbacks) {
    this.id = ''
    this.data = {
      translations: {}
    }
    this.callbacks = callbacks
  }
  displayData(tuple) {
    this.data = Object.assign(this.data, tuple)
    this.draw()
  }
  draw(){
    throw('Implement #draw method')
  }
  translate(translations) {
    this.displayData({ translations: translations })
  }
}

export default Renderer
