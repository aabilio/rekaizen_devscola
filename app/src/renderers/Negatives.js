import React from 'react'
import ReactDOM from 'react-dom'
import Renderer from './Renderer'
import ColumnAnalysis from '../components/ColumnAnalysis'

class Negatives extends Renderer {
  constructor (callbacks) {
    super (callbacks)
    this.id = 'negatives'
    this.data = {
      showTextArea: false,
      translations: {
        add: '',
        listTitle: ''
      },
      collection: []
    }
  }
  translate (payload) {
    const translations = {
      add: payload.addNegative,
      listTitle: payload.negatives
    }
    this.displayData({ translations: translations })
  }

  update (collection) {
    this.displayData({ collection: collection })
  }

  updateOne (negative) {
    this.data.collection.push(negative)
    this.update(this.data.collection)
  }

  hideTextArea (){
    this.displayData({ showTextArea: false })
  }

  showTextArea () {
    this.displayData({ showTextArea: true })
  }

  draw () {
    ReactDOM.render(
      <ColumnAnalysis
        icon={<i className="fas fa-arrow-down icon" />}
        id={this.id}
        translations={this.data.translations}
        collection={this.data.collection}
        onClick={this.showTextArea.bind(this)}
        create={this.callbacks.create}
        remove={this.callbacks.remove}
        hide={this.hideTextArea.bind(this)}
        show={this.data.showTextArea}
      />,
      document.querySelector('#' + this.id)
    )
  }
}

export default Negatives
