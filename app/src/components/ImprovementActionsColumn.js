import React from 'react'
import List from './List'
import Button from './Button'
import TextareaAutosize from './TextareaAutosize'


class ImprovementActionsColumn extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      visible: false,
    }
  }

  show() {
    this.setState({ visible: true })
  }

  hide() {
    this.setState({ visible: false })
  }

  cancel() {
    this.setState({
      visible: false,
      value: '',
    })
  }

  handleChange(event) {
    this.setState({ value: event.target.value })
  }

  handleSubmit(event) {
    event.preventDefault()
    if(this.state.value) {
      this.props.create(this.state.value)
      this.setState({
        value: '',
        visible: false,
     })
    }
  }

  render() {
    return (
      <div className="column-inner">
        <div className="list-header">
          <div className="list-title">
            {this.props.translations.listTitle}
          </div>
          {!this.state.visible &&
            <Button
                id={`${this.props.id}-show`}
                secondary
                onClick={this.show.bind(this)}
              >
              {this.props.translations.add}
            </Button>
          }
        </div>
        {this.state.visible &&
          <React.Fragment>
            <div className="background-cover" onClick={this.hide.bind(this)}></div>
            <form
              className="background-highlight has-text-right no-print"
              onSubmit={this.handleSubmit.bind(this)}
            >
              <TextareaAutosize
                autoFocus
                value={this.state.value}
                onChange={this.handleChange.bind(this)}
              />
              <Button
                text
                className="has-text-grey"
                id={`${this.props.id}-cancel`}
                onClick={this.cancel.bind(this)}
              >
                <i className="fas fa-times" />
              </Button>
              <Button
                secondary
                type="submit"
                id={`${this.props.id}-add`}
              >
                <i className="fas fa-check" />
              </Button>
            </form>
          </React.Fragment>
        }
        <List
          bordered
          id={this.props.id}
          remove={this.props.remove}
          collection={this.props.collection}
          icon={<i className="icon fas fa-level-up-alt" />}
        />
      </div>
    )
  }
}

export default ImprovementActionsColumn
