import React from 'react'
import classNames from '../libraries/classNames'

class Button extends React.Component {
  render() {
    const { className, ceremonial, ending, primary, secondary, link, text, ...props } = this.props
    const classes = classNames('button no-print', {
      'is-ceremonial': ceremonial,
      'is-ending': ending,
      'is-primary': primary,
      'is-secondary': secondary,
      'is-link': link,
      'is-text': text,
    }, className)

    return (
      <button
        className={classes}
        {...props}
      >
        {this.props.children}
      </button>
    )
  }
}

Button.defaultProps = {
  ceremonial: false,
  ending: false,
  primary: false,
  secondary: false,
  link: false,
  text: false,
  className: ''
}

export default Button
