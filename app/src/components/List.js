import React from 'react'
import Button from './Button'
import classNames from '../libraries/classNames'

class List extends React.Component {
  removeButton(item) {
    if(this.props.remove) {
      return (
        <Button
          text
          onClick={() => this.props.remove(item)}
        >
          <i className="fa fa-trash" />
        </Button>
      )
    }
    return null
  }
  rows() {
    return this.props.collection
      .map((item, index) => (
        <li
          key={item.id || item.birthday || item.description}
          id={this.props.id + '-' + index}
          className="list-item"
        >
          {this.props.icon}
          <span>{item.description}</span>
          {this.removeButton(item)}
        </li>
      )
    )
  }
  render() {
    const { bordered, className } = this.props
    const classes = classNames('rekaizen-list', {
      bordered: bordered,
    }, className)

    return (
      <ul className={classes} id={this.props.id + "-list"}>
        {this.rows()}
      </ul>
    )
  }
}

List.defaultProps = {
  collection: [],
  bordered: false,
  icon: null,
}

export default List
