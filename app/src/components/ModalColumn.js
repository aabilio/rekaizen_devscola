import React from 'react'
import Button from './Button'
import TextareaAutosize from './TextareaAutosize'

class ModalColumn extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      description: ''
    }
  }

  handleChange(event) {
    this.setState({ description: event.target.value })
  }

  handleSubmit(event) {
    event.preventDefault()
    const description =  this.state.description
    if(description.length) {
      this.props.onSubmit(description)
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="modal-column-background"></div>
        <form onSubmit={this.handleSubmit.bind(this)} className="modal-column">
          <Button
            text
            className="close"
            onClick={this.props.onClose}
          >
            <i className="fas fa-times fa-lg has-text-grey-dark"/>
          </Button>
          <h2 className="title has-text-white">
            {this.props.title}
          </h2>
          <TextareaAutosize
            autoFocus
            rows="5"
            className="textarea dark"
            id="modal-column-description"
            name="description"
            value={this.state.description}
            onChange={this.handleChange.bind(this)}
          />
          <p className="text text-ceremonial">
            {this.props.textHelp}
          </p>
          <Button
            ceremonial
            type="submit"
          >
            {this.props.textSubmit}
          </Button>
        </form>
      </React.Fragment>
    )
  }
}

ModalColumn.defaultProps = {
  title: '',
  textHelp: '',
  textSubmit: '',
  show: true,
}

export default ModalColumn
