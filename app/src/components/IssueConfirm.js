import React from "react"
import LogoRe from './LogoRe'
import Button from './Button'

class IssueConfirm extends React.Component {
  render() {
    return (
      <div className="slide">
        <LogoRe className="hidden" />
        <div className="issue-confirm-title">
          <div>
            <LogoRe height="89" width="92" time={this.props.timebox} />
          </div>
          <div>
            <h2 className="title">{this.props.description}</h2>
          </div>
        </div>
        <div className="modal-container">
          <p className="text text-ceremonial">
            {this.props.translations.confirmIssueSubtitle}
          </p>
          <Button
            id='confirm-issue'
            ceremonial
            onClick={this.props.onSubmit}
          >
            {this.props.translations.confirmIssue}
          </Button>
        </div>
        <div className="modal-footer">
          <Button
            link
            onClick={this.props.back}
          >
            {this.props.translations.back}
          </Button>
        </div>
      </div>
    )
  }
}


export default IssueConfirm
