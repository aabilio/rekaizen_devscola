import React from 'react'
import Modal from './Modal'
import Button from './Button'
import LogoRe from './LogoRe'
import ReportContent from './ReportContent'
import ReportMarkdown from './ReportMarkdown'

class ReportModal extends Modal {
  handlePrint() {
    window.document.title = this.props.issueDescription
    window.print()
  }

  finishRetro() {
    this.props.finishRetro()

    window.location.href = window.location.href
  }

  content() {
    return (
      <React.Fragment>
        <LogoRe />
        <div className="modal-container">
          <ReportContent
            issueDescription={this.props.issueDescription}
            translations={this.props.translations}
            explanation={this.props.explanation}
          />
          <p className="has-text-centered">
            <button
              id="button-summary"
              className="button-report"
              onClick={this.handlePrint.bind(this)}
            >
              <div className="justify-icon">
                <i className="fas fa-file-download fa-5x" />
              </div>
              <div className="opacity-layer"></div>
              <img src="images/board-scale.png" />
            </button>
          </p>
          <p className="has-text-centered">
            <Button
              ceremonial
              id={`${this.props.id}-new-issue`}
              onClick={this.props.newIssue}
            >
              {this.props.translations.newIssue}
            </Button>
          </p>
          <p className="has-text-centered">
            <Button
              ending
              id={`${this.props.id}-finish-retro`}
              onClick={this.finishRetro.bind(this)}
            >
              {this.props.translations.finishRetro}
            </Button>
          </p>
          <ReportMarkdown
            issueDescription={this.props.issueDescription}
            translations={this.props.translations}
            explanation={this.props.explanation}
          />
        </div>
      </React.Fragment>
    )
  }
}

export default ReportModal
