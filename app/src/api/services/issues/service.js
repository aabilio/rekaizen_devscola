import Collection from './collection'
import Issue from '../../domain/Issue'
import Analysis from '../../domain/Analysis'

export default class Service {
  static create(description, timebox) {
    const issue = new Issue(description, timebox)
    const stored = Collection.store(issue)
    return stored.serialize()
  }

  static retrieve(issue) {
    const retrieved = Collection.retrieve(issue)

    return retrieved.serialize()
  }

  static addConclusion(issue, conclusion) {
    const retrieved = Collection.retrieve(issue)

    retrieved.addConclusion(conclusion)

    return retrieved.serialize()
  }

  static addImprovementAction(issue, id) {
    const retrieved = Collection.retrieve(issue)

    const improvementAction = retrieved.addImprovementAction(id)

    return improvementAction
  }

  static retrieveImprovementActions(issue) {
    const retrieved = Collection.retrieve(issue)
    return retrieved.retrieveImprovementActions()

  }

  static removeImprovementAction(issue, id) {
    const retrieved = Collection.retrieve(issue)
    retrieved.removeImprovementAction(id)
    return retrieved.serialize()
  }

  static createNegative(issue, description) {
    const retrieved = Collection.retrieve(issue)
    const analysis = Analysis.asNegative(description)
    retrieved.addAnalysis(analysis)

    return analysis.serialize()
  }

  static createPositive(issue, description) {
    const retrieved = Collection.retrieve(issue)
    const analysis = Analysis.asPositive(description)
    retrieved.addAnalysis(analysis)

    return analysis.serialize()
  }

  static retrievePositives (issue) {
    const retrieved = Collection.retrieve(issue)
    const positives = retrieved.retrievePositives()

    return positives.map(positive => positive.serialize())
  }

  static retrieveNegatives (issue) {
    const retrieved = Collection.retrieve(issue)
    const negatives = retrieved.retrieveNegatives()

    return negatives.map(negative => negative.serialize())
  }

  static removeAnalysis(issueDescription, analysis) {
    const issue = Collection.retrieve(issueDescription)

    issue.removeAnalysis(analysis)

    return issue.serialize()
  }

  static finalize(id) {
    const issue = Collection.retrieve(id)

    issue.finalize()

    return issue.serialize()
  }

  static retrieveAll() {
    const issues = Collection.retrieveAll()
    const serialized = issues.map(issue => issue.serialize())

    return serialized
  }
}
