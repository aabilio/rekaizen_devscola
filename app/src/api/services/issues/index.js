import Collection from './collection'
import Service from './service'

const Issues = {
  collection: Collection,
  service: Service
}

export default Issues
