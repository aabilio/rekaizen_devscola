import Collection from './collection'
import Service from './service'

const Conclusions = {
  collection: Collection,
  service: Service
}

export default Conclusions
