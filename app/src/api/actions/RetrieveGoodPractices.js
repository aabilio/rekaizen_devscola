import Conclusions from '../services/conclusions'
import Issues from '../services/issues'

export default class RetrieveGoodPractices {
  static do(issue) {
    const retrieved = Issues.service.retrieve(issue)

    return Conclusions.service.retrieveAllGoodPractices(retrieved.conclusions)
  }
}
