import Issues from '../services/issues'

export default class CreatePositive {
  static do(positive) {
    const newPositive = Issues.service.createPositive(positive.issue, positive.description)

    return newPositive
  }
}
