import Timeboxes from '../services/timeboxes'

export default class RetrieveTimeBoxes {
  constructor(bus) {
    this.bus = bus

    this.do()
  }

  do() {
    const timeBoxOptions = Timeboxes.service.retrieveAll()

    this.bus.publish('got.timeboxes', timeBoxOptions)
  }
}
