import ImprovementActions from '../services/improvementActions'
import Issues from '../services/issues'

export default class RemoveImprovementAction {
  static do(improvementAction) {
    ImprovementActions.service.remove(improvementAction.id)

    const issue = Issues.service.removeImprovementAction(improvementAction.issue, improvementAction.id)

    return issue
  }
}
