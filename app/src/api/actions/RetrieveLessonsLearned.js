import Conclusions from '../services/conclusions'
import Issues from '../services/issues'

export default class RetrieveLessonsLearned {
  constructor(bus) {
    this.bus = bus

    this.bus.subscribe('retrieve.lessonsLearned', this.do.bind(this))
  }

  do({ issue }) {
    const retrieved = Issues.service.retrieve(issue)

    const lessonsLearned = Conclusions.service.retrieveAllLessonsLearned(retrieved.conclusions)

    this.bus.publish('retrieved.lessonsLearned', lessonsLearned)
  }

  static do(issue) {
    const retrieved = Issues.service.retrieve(issue)
    return Conclusions.service.retrieveAllLessonsLearned(retrieved.conclusions)
  }
}
