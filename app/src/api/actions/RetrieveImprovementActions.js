import ImprovementActions from '../services/improvementActions'
import Issues from '../services/issues'

export default class RetrieveImprovementActions {
  static do(issue) {
    const ids = Issues.service.retrieveImprovementActions(issue)
    const improvementActions = ImprovementActions.service.retrieve(ids)

    return {
      improvementActions: improvementActions
    }
  }
}
