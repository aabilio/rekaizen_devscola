import Issues from '../services/issues'

export default class RetrieveAnalyses {
  static do(issue) {
    const positives = Issues.service.retrievePositives(issue.issue)
    const negatives = Issues.service.retrieveNegatives(issue.issue)

    return { positives, negatives }
  }

  constructor(bus) {
    this.bus = bus

    this.bus.subscribe('retrieve.negatives', this.do.bind(this))
  }

  do(issue) {
    const negatives = Issues.service.retrieveNegatives(issue.issue)

    this.bus.publish('retrieved.negatives', negatives)
  }
}
