import Dates from '../libraries/Dates'
import Interpolate from '../libraries/Interpolate'

export default class Report {
  constructor(issue, translations) {
    this.translations = translations
    this.issue = issue
    this.positivesCount = 0
    this.negativesCount = 0
    this.conclusionsCount = issue.conclusions.length
    this.goodPractices = []
    this.lessonsLearned = []
    this.improvementActions = []
  }

  addDataForIntroduction(positivesCount, negativesCount) {
    this.positivesCount = positivesCount
    this.negativesCount = negativesCount
  }

  addDataForGoodPractices(goodPractices) {
    this.goodPractices = goodPractices
  }

  addDataForLessonsLearned(lessonsLearned) {
    this.lessonsLearned = lessonsLearned
  }

  addDataForImprovementActions(improvementActions) {
    this.improvementActions = improvementActions
  }

  generateIntroduction() {
    const firstIntroductionSentence = this.writeFirstIntroductionSentence()
    const analysesSentence = this.writeAnalysesSentence()
    const conclusions = this.conclusionsSentence()

    return `${firstIntroductionSentence}${analysesSentence} ${conclusions}`
  }

  prepareDescriptionsList(allConclusions) {
    let conclusions = []

    conclusions = allConclusions.map(conclusion => {
      return conclusion.description
    })

    return conclusions
  }

  prepareImprovementActionsList (allImprovementActions) {
    return allImprovementActions.map(improvementAction => improvementAction.description)

  }

  serialize() {
    return {
      'title': this.translations.reportTitle,
      'introduction': this.generateIntroduction(),
      'goodPracticesIntro': this.generateGoodPracticesIntro(),
      'goodPractices': this.prepareDescriptionsList(this.goodPractices),
      'lessonsLearnedIntro': this.generateLessonsLearnedIntro(),
      'lessonsLearned': this.prepareDescriptionsList(this.lessonsLearned),
      'improvementActionsIntro': this.generateImprovementActionsIntro(),
      'improvementActions': this.prepareImprovementActionsList(this.improvementActions),
      'ending': this.generateEnding()
    }
  }

  writeFirstIntroductionSentence() {
    const description = this.issue.description
    const introduction = this.translations.introduction

    const firstIntroductionSentence = Interpolate
      .withKeys(introduction, { description })

    return firstIntroductionSentence
  }

  generateLessonsLearnedIntro() {
    const lessonsLearnedCount = this.lessonsLearned.length
    let sentence = ''

    if (this.isPlural(lessonsLearnedCount)) { sentence = this.translations.lessonsLearnedSentenceForMore }
    if (this.isSingular(lessonsLearnedCount)) { sentence = this.translations.lessonsLearnedSentenceForOne }

    return sentence
  }

  generateGoodPracticesIntro() {
    const goodPracticesCount = this.goodPractices.length
    let sentence = ''

    if (this.isPlural(goodPracticesCount)) { sentence = this.translations.goodPracticesSentenceForMore}
    if (this.isSingular(goodPracticesCount)) { sentence = this.translations.goodPracticesSentenceForOne }

    return sentence
  }

  generateImprovementActionsIntro () {
    const improvementActionsCount = this.improvementActions.length
    let sentence = ''

    if (this.isPlural(improvementActionsCount)) {
      sentence = this.translations.improvementActionsSentenceForMore
    }
    if (this.isSingular(improvementActionsCount)) {
      sentence = this.translations.improvementActionsSentenceForOne
    }

    return sentence
  }

  writeAnalysesSentence () {
    const positivesCount = this.positivesCount
    const negativesCount = this.negativesCount
    let sentence = this.translations.notAnalysesSentence

    if (positivesCount > 0 || negativesCount > 0) {
      sentence = `${this.positivesImpresionsSentence()} ${this.negativesImpresionsSentence()}`
    }

    return sentence
  }

  positivesImpresionsSentence() {
    const positivesCount = this.positivesCount
    let sentence = this.translations.positivesImpresionsSentenceForZero

    if (this.isPlural(positivesCount)) { sentence = this.writePluralImpresionSentence(positivesCount, this.translations.positivesImpresionsSentenceForMore) }
    if (this.isSingular(positivesCount)) { sentence = this.translations.positivesImpresionsSentenceForOne}

    return sentence
  }

  negativesImpresionsSentence() {
    const negativesCount = this.negativesCount
    let sentence = this.translations.negativeImpressionsSentenceForZero

    if (this.isPlural(negativesCount)) { sentence = this.writePluralImpresionSentence(negativesCount, this.translations.negativeImpressionsSentenceForMore) }
    if (this.isSingular(negativesCount)) { sentence = this.translations.negativeImpressionsSentenceForOne }

    return sentence
  }

  conclusionsSentence() {
    const conclusionsCount = this.conclusionsCount
    let sentence = this.translations.conclusionsSentenceForZero

    if (this.isPlural(conclusionsCount)) { sentence = this.translations.conclusionsSentenceForMore }
    if (this.isSingular(conclusionsCount)) { sentence = this.translations.conclusionsSentenceForOne }

    return sentence
  }

  isSingular(count) {
    return (count == 1)
  }

  isPlural(count) {
    return (count > 1)
  }

  writePluralImpresionSentence (number, sentence) {
    return this.interpolate(sentence, { number })
  }

  generateEnding() {
    const endingDate = this.writeEndingDate()
    const endingTime = this.writeEndingTime()

    return `${endingDate} ${endingTime}`
  }

  writeEndingDate() {
    const stringDate = this.prepareDate()
    const endingDateNoVars = this.translations.endingDate

    return this.interpolate(endingDateNoVars, { stringDate })
  }

  writeEndingTime() {
    const spentTime = this.calculateSpentTime()
    const timebox = this.issue.timebox
    const endingTimeNoVars = this.translations.endingTime
    const timeboxWithMinute = this.timeWithMinute(timebox)
    const spentTimeWithMinute = this.timeWithMinute(spentTime)

    const endingTime = Interpolate
      .withKeys(endingTimeNoVars, { timebox: timeboxWithMinute, spentTime: spentTimeWithMinute })

    return endingTime
  }

  timeWithMinute(time){
    const timeWithMinutes = this.translations.timeWithMinutes
    let preparedTime  = this.interpolate(timeWithMinutes, { time })

    if (this.isSingular(time)) {
      const timeWithMinute = this.translations.timeWithMinute
      preparedTime = this.interpolate(timeWithMinute, { time })
    }

    return preparedTime
  }

  calculateSpentTime() {
    return Dates.timeLapsed(this.issue.birthday)
  }

  prepareDate() {
    return Dates.dateToString(this.issue.birthday)
  }

  interpolate (text, strings) {
    return Interpolate.withKeys(text, strings)
  }
}
