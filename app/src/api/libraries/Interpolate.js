
export default class Interpolate {
  static withKeys (text, strings) {
    const keys = Object.keys(strings)
    let result = text

    keys.forEach(key => {
      result = result.replace(`%${key}`, strings[key])
    })

    return result
  }
}
