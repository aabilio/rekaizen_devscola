
export default class Dates {
  static dateToString(timestamp) {
    const formattedDate = new Date(timestamp)
    const day = formattedDate.getDate()
    const month = formattedDate.getMonth() + 1
    const year = formattedDate.getFullYear()

    return `${day}/${month}/${year}`
  }

  static timeLapsed(timestamp) {
    const now = new Date()
    const spentTime = now - new Date(timestamp)
    const spentMinutes = parseInt(spentTime/60000)
    return spentMinutes
  }

  static generateTimestamp() {
    return new Date().getTime()
  }
}
