import Issues from '../../../src/api/services/issues'
import Subscriber from '../../services/subscriber'
import Issue from '../../../src/api/domain/Issue'
import Bus from '../../../src/infrastructure/bus'
import Actions from '../../../src/api/actions'

describe('RetrieveLessonsLearned Action', () => {
  let bus, issue, description

  beforeEach(() => {
    bus = new Bus()

    issue = new Issue('description', 45)
    Issues.collection.store(issue)
    description = 'A Lesson Learned'
  })

  it('retrieves the lessons learned', () => {
    const subscriber = new Subscriber('retrieved.lessonsLearned', bus)
    Actions.retrieveLessonsLearned(bus)
    createLessonLearned({ description: description, issue: issue.id })

    bus.publish('retrieve.lessonsLearned', { issue: issue.id })

    expect(subscriber.hasBeenCalledWith().length).toBe(1)
    expect(subscriber.hasBeenCalledWith()[0].description).toBe(description)
  })

  function createLessonLearned({ description, issue }) {
    Actions.createLessonLearned(bus)

    bus.publish('create.lessonLearned', { description, issue })
  }
})
