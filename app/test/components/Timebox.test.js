import React from 'react'
import {render} from 'react-testing-library'
import Timebox from '../../src/components/Timebox'

describe('Timebox', () => {
  test('logo and loader load correctly', () => {
    const timebox = render(<Timebox timebox={15} />).container
    const logoSVG = timebox.querySelector('.logo svg')
    const loaderSVG = timebox.querySelector('.logo-loader svg')

    expect(logoSVG).not.toBeNull()
    expect(loaderSVG).not.toBeNull()
  })
})
