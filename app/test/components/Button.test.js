import Button from '../../src/components/Button'
import {render, fireEvent, cleanup} from 'react-testing-library'
import Callback from './CallBack'
import React from 'react'

describe('Button', () => {
  beforeEach(cleanup)

  it('executes any callback when is clicked', () => {
    const callBack = new Callback()
    const text = 'Any text'
    const button = render(
      <Button
        onClick={callBack.toBeCalled.bind(callBack)}
      >
        {text}
      </Button>
    ).getByText

    fireEvent.click(button(text))

    expect(callBack.hasBeenCalled()).toBeTruthy()
  })

  it('has class is-ceremonial when pass a prop ceremonial', () => {
    const container = render(<Button ceremonial />).container

    const button = container.querySelector('button')

    expect(button.classList.contains('is-ceremonial')).toBeTruthy()
  })
})
