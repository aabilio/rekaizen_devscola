import Orderer from "../../src/libraries/Orderer";


describe('Orderer', () => {
  it('by birthday the newest item first', () => {
    const newest = { birthday: 1561453075443 }
    const oldest = { birthday: 1561453072289 }
    const collection = [oldest, newest]

    const result = Orderer.byBirthdayNewestToOldest(collection)

    expect(result).toEqual([newest, oldest])
  })
  it('by birthday the oldest item first', () => {
    const newest = { birthday: 1561453075443 }
    const oldest = { birthday: 1561453072289 }
    const collection = [newest, oldest]

    const result = Orderer.byBirthdayOldestToNewest(collection)

    expect(result).toEqual([oldest, newest])
  })
})