import GoodPractices from '../../src/services/goodPractices'
import Issues from '../../src/api/services/issues'
import Issue from '../../src/api/domain/Issue'
import Bus from '../../src/infrastructure/bus'
import Subscriber from './subscriber'

describe('Good Practices Service', () => {
  let bus, issue, description

  beforeEach(() => {
    bus = new Bus()

    issue = new Issue('description', 45)
    Issues.collection.store(issue)
    description = 'A Good Practice'
  })

  it('creates a good practice', () => {
    const subscriber = new Subscriber('created.goodPractice', bus)
    new GoodPractices(bus)

    bus.publish('create.goodPractice', { description: description, issue: issue.id })

    expect(subscriber.hasBeenCalledWith().description).toBe(description)
  })

  it('retrieves the good practices', () => {
    const subscriber = new Subscriber('retrieved.goodPractices', bus)
    new GoodPractices(bus)
    createGoodPractice({ description, issue: issue.id })

    bus.publish('retrieve.goodPractices', { issue: issue.id })

    expect(subscriber.hasBeenCalledWith().length).toBe(1)
    expect(subscriber.hasBeenCalledWith()[0].description).toBe(description)
  })

  function createGoodPractice({ description, issue }) {
    bus.publish('create.goodPractice', { description, issue })
  }
})
