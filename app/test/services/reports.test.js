import Reports from '../../src/services/reports'
import Positives from '../../src/services/positives'
import GoodPractices from '../../src/services/goodPractices'
import ImprovementActions from '../../src/services/ImprovementActions'
import Issues from '../../src/api/services/issues'
import Issue from '../../src/api/domain/Issue'
import Actions from '../../src/api/actions'
import Bus from '../../src/infrastructure/bus'
import Subscriber from './subscriber'

describe('Reports', () => {
  let bus

  beforeEach(() => {
    bus = new Bus()
  })

  it('retrieves a report', () => {
    const issue = createFilledIssue()
    const subscriber = new Subscriber('generated.report', bus)
    new Reports(bus)

    bus.publish('generate.report', { issue: issue.id })

    const report = subscriber.hasBeenCalledWith()
    expect(report.ending.length).not.toBe(0)
    expect(report.goodPractices.length).toBe(1)
    expect(report.lessonsLearned.length).toBe(1)
    expect(report.improvementActions.length).toBe(1)
  })

  function createFilledIssue() {
    const issue = new Issue('issue', 45)
    Issues.collection.store(issue)
    new Positives(bus)
    new GoodPractices(bus)
    new ImprovementActions(bus)
    Actions.removeAnalysis(bus)
    Actions.retrieveAnalyses(bus)
    Actions.createNegative(bus)
    Actions.createLessonLearned(bus)
    bus.publish('create.negative', { issue: issue.id, description: 'negative' })
    bus.publish('create.positive', { issue: issue.id, description: 'positive' })
    bus.publish('create.goodPractice', { issue: issue.id, description: 'goodPractice' })
    bus.publish('create.improvementAction', { issue: issue.id, description: 'improvementAction' })
    bus.publish('create.lessonLearned',{ issue: issue.id, description: 'lessonLearned' })

    return issue
  }
})
