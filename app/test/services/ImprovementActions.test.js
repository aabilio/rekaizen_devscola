import ImprovementActions from '../../src/services/ImprovementActions'
import Issues from '../../src/api/services/issues'
import Issue from '../../src/api/domain/Issue'
import Bus from '../../src/infrastructure/bus'
import Subscriber from './subscriber'

describe('Good Practices Service', () => {
  let bus, issue, description, issueDescription

  beforeEach(() => {
    bus = new Bus()

    issue = new Issue('description', 45)
    Issues.collection.store(issue)
    description = 'An Improvement Action'
  })

  it('creates a good practice', () => {
    const subscriber = new Subscriber('created.improvementAction', bus)
    new ImprovementActions(bus)

    bus.publish('create.improvementAction', { description: description, issue: issue.id })

    expect(subscriber.hasBeenCalledWith().description).toBe(description)
  })

  it('retrieves the good practices', () => {
    const subscriber = new Subscriber('retrieved.improvementActions', bus)
    new ImprovementActions(bus)
    createImprovementAction({ description, issue: issue.id })

    bus.publish('retrieve.improvementActions', { issue: issue.id })

    expect(subscriber.hasBeenCalledWith().improvementActions.length).toBe(1)
    expect(subscriber.hasBeenCalledWith().improvementActions[0].description).toBe(description)
  })

  it('removes an Improvement Action', () => {
    const subscriber = new Subscriber('retrieved.improvementActions', bus)
    new ImprovementActions(bus)
    createImprovementAction({ description, issue: issue.id })
    bus.publish('retrieve.improvementActions', { issue: issue.id })
    const improvementAction = subscriber.hasBeenCalledWith().improvementActions[0]

    bus.publish('remove.improvementAction', { issue: issue.id, id: improvementAction.id })

    bus.publish('retrieve.improvementActions', { issue: issue.id })
    expect(subscriber.hasBeenCalledWith().improvementActions.length).toBe(0)
  })

  function createImprovementAction({ description, issue }) {
    bus.publish('create.improvementAction', { description, issue })
  }
})
